package com.github.carlosrf.datageneration.util;

import com.github.carlosrf.datageneration.exception.InvalidArgumentException;
import com.github.carlosrf.datageneration.model.File;
import com.github.carlosrf.datageneration.model.Folder;
import com.google.common.collect.Lists;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.github.carlosrf.datageneration.util.DataGenerationUtils.*;
import static org.junit.Assert.assertEquals;

public class DataGenerationUtilsTest {

    @Test
    public void extractLongForLongValueTest() {
        long response = extractLong("12", "No error should be thrown");
        assertEquals(12l, response);
    }

    @Test(expected = InvalidArgumentException.class)
    public void extractLongForNonLongValueTest() {
        extractLong("a12", "No error should be thrown");
    }

    @Test
    public void parseFoldersTest() throws ParseException {

        ArrayList<Folder> expected = Lists.newArrayList(
                Folder.of("locations", 20l, Lists.newArrayList()),
                Folder.of("devices", 23l, Lists.newArrayList()));

        List<Folder> result = parseFolders(new String[]{"locations", "20", "devices", "23"});

        assertEquals(expected, result);
    }

    @Test(expected = InvalidArgumentException.class)
    public void parseFoldersMissingInformationTest() throws ParseException {
        List<Folder> result = parseFolders(new String[]{"locations", "20", "devices"});
    }

    @Test
    public void buildFileInfoExactFilesTest() {
        long expectedSize = 15;
        List<File> files = buildFileInfo(3l, expectedSize);

        long size = files.stream().collect(Collectors.summingLong(File::getSize));

        assertEquals(expectedSize, size);
    }

    @Test
    public void buildFileInfoNotExactFilesTest() {
        long expectedSize = 14;
        List<File> files = buildFileInfo(3l, expectedSize);

        long size = files.stream().collect(Collectors.summingLong(File::getSize));

        assertEquals(expectedSize, size);
    }

}