package com.github.carlosrf.datageneration.util;

import com.github.carlosrf.datageneration.DataGeneration;
import com.github.carlosrf.datageneration.exception.InvalidArgumentException;
import com.github.carlosrf.datageneration.model.File;
import com.github.carlosrf.datageneration.model.Folder;
import com.google.common.collect.Lists;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Utility class for data generation application.
 */
public class DataGenerationUtils {
    /**
     * Multiplication factor to transform megabytes into bytes.
     */
    public static Long MB_TO_BYTES_FACTOR = 1048576l;

    /**
     * Parses a string into a long throwing an exception when it is not possible.
     * @param value the value to be parsed.
     * @param messageError the message to be used in the exception in case of failure.
     * @return a long parsed form the string
     * @throws InvalidArgumentException if not possible to parse
     */
    public static Long extractLong(String value, String messageError) throws InvalidArgumentException {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new InvalidArgumentException(e.getMessage());
        }
    }

    /**
     * Reads the location folder structure and creates a DataGeneration representation of it.
     *
     * @param location location to be read.
     * @return a DataGeneration object representing the location structure.
     * @throws ParseException if some problem happens when reading the structure.
     */
    public static DataGeneration buildDataGeneration(String location) throws ParseException {
        List<Folder> folders = parseFolders(location);
        Long fileSize = extractFileSize(folders);

        return DataGeneration.of(fileSize, location, folders);
    }

    /**
     * Creates a datageneration representation to be used in master dataset creation..
     *
     * @param location location to create the dataset.
     * @return a DataGeneration object representing the location structure.
     * @throws ParseException if some problem happens when parsing elements.
     */
    public static DataGeneration buildDataGeneration(Long fileSize, String location, String[] foldersArray) throws ParseException {
        List<Folder> folders = parseFolders(buildFoldersInfoList(foldersArray), fileSize);

        return DataGeneration.of(fileSize, location, folders);
    }

    /**
     * Return a list of files of size fileSize necessary to fulfil the size.
     * @param fileSize file size to be used.
     * @param size size to be fulfilled.
     * @return a list of files necessary summing up size.
     */
    public static List<File> buildFileInfo(final Long fileSize, final Long size) {
        final Integer fullFilesSize = size.intValue() / fileSize.intValue();

        List<File> files = Collections.nCopies(fullFilesSize, fileSize).stream()
                .map(s -> File.of(UUID.randomUUID().toString(), s)).collect(toList());

        Long lastFileSize = size % fileSize;
        if (lastFileSize != 0) {
            files.add(File.of(UUID.randomUUID().toString(), lastFileSize));
        }
        return files;
    }

    /**
     * Parses folder names argument from command line and creates a list of Folders with its respective name and size.
     *
     * @param foldersArray folders information array
     * @return a list of folders representation.
     * @throws ParseException if some error happens when parsing.
     */
    public static List<Folder> parseFolders(final String[] foldersArray) throws ParseException {
        ArrayList<String> foldersInfo = buildFoldersInfoList(foldersArray);

        return Lists.partition(foldersInfo, 2).stream()
                .map(t -> {
                    Long size = extractLong(t.get(1), "Error on parse folder size for folders-size arg");

                    List<File> files = Lists.newArrayList();

                    return Folder.of(t.get(0), size, files);
                }).collect(toList());
    }

    private static ArrayList<String> buildFoldersInfoList(String[] foldersArray) {
        if (foldersArray.length % 2 != 0) {
            throw new InvalidArgumentException("Folders size argument must follow the pattern <name1>,<size1>,<name2>,<size2>, ...");
        }

        return Lists.newArrayList(foldersArray);
    }

    private static List<Folder> parseFolders(String location) throws ParseException {
        java.io.File baseDir = new java.io.File(location);

        if (!baseDir.exists()) {
            throw new ParseException("Location directory does not exists");
        }

        return Lists.newArrayList(baseDir.listFiles()).stream().map(folder -> {
            List<File> files = Lists.newArrayList(folder.listFiles())
                    .stream().map(file -> File.of(file.getName(), file.length() / MB_TO_BYTES_FACTOR))
                    .collect(toList());

            Long folderSize = files.stream()
                    .collect(Collectors.summingLong(File::getSize));


            return Folder.of(folder.getName(), folderSize, files);
        }).collect(toList());
    }

    private static Long extractFileSize(List<Folder> folders) {
        Double size = folders.stream().map(Folder::getFiles).flatMap(Collection::stream)
                .collect(Collectors.averagingLong(File::getSize));

        return Math.round(size);
    }

    private static List<Folder> parseFolders(final List<String> foldersInfo, final Long fileSize) throws ParseException {
        return Lists.partition(foldersInfo, 2).stream()
                .map(t -> {
                    Long size = extractLong(t.get(1), "Error on parse folder size for folders-size arg");

                    List<File> files = buildFileInfo(fileSize, size);

                    return Folder.of(t.get(0), size, files);
                }).collect(toList());
    }

}
