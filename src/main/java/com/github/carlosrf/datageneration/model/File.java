package com.github.carlosrf.datageneration.model;

import lombok.Value;

/**
 * File representation
 */
@Value(staticConstructor="of")
public class File {
    String name;
    Long size;
}
