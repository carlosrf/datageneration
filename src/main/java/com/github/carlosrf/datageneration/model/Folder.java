package com.github.carlosrf.datageneration.model;

import lombok.Value;

import java.util.List;

/**
 * Folder representation
 */
@Value(staticConstructor="of")
public class Folder {
    String name;
    Long size;

    List<File> files;
}
