package com.github.carlosrf.datageneration;

import com.github.carlosrf.datageneration.exception.FileCreationException;
import com.github.carlosrf.datageneration.model.File;
import com.github.carlosrf.datageneration.model.Folder;
import lombok.Value;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.github.carlosrf.datageneration.util.DataGenerationUtils.MB_TO_BYTES_FACTOR;
import static com.github.carlosrf.datageneration.util.DataGenerationUtils.buildFileInfo;
import static java.lang.Math.min;

/**
 * Represents a dataset info and performs operation on that dataset like create, update and backup.
 */
@Value(staticConstructor="of")
public class DataGeneration {
    Long fileSize;
    String path;
    List<Folder> folders;

    /**
     * Creates the files for the dataset.
     */
    public void createFiles() {
        createFolders(path);

        folders.forEach(folder -> {
            createFolders(String.format("%s/%s", path, folder.getName()));
            folder.getFiles().forEach(file -> createFile(file, folder));
        });
    }

    private void createFolders(String path) {
        try {
            Files.createDirectories(Paths.get(path));
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            throw new FileCreationException("Location directory could not be created");
        }
    }

    /**
     * Updates master dataset on location based on folders information.
     * @param folders information about update for folders.
     */
    public void update(List<Folder> folders) {
        folders.forEach(folder -> {
            this.folders.stream().filter( f-> f.getName().equals(folder.getName()))
                    .findFirst().ifPresent(f -> updateFolder(f, folder));
        });
    }

    /**
     * Create a copy of the master dataset into the destination folder.
     * @param destination place to create a copy.
     */
    public void backup(String destination) {
        Path destinationPath = Paths.get(destination);
        try {
            if (Files.exists(destinationPath)) {
                Files.move(destinationPath, Paths.get(String.format("%s_%s", destination, System.currentTimeMillis())));
            }

            createFolders(destination);

            // follow links when copying files
            EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
            Path source = Paths.get(path);
            BackupFiles backupFiles = BackupFiles.of(source, destinationPath);

            Files.walkFileTree(source, opts, Integer.MAX_VALUE, backupFiles);

        } catch (IOException e) {
            throw new FileCreationException("Backup directory could not be created");
        }
    }

    private String getPath(String fileName, String folder) {
        return String.format("%s/%s/%s", path, folder, fileName);
    }

    /**
     * Creates a file on fs using random content with the size specified in file parameter.
     *
     * @param file the file info to be created.
     * @param folder the folder in wich the file.
     *
     * @throws FileCreationException if the file could not be created.
     */
    public void createFile(File file, Folder folder) throws FileCreationException {
        PrintWriter writer = null;
        try{
            writer = new PrintWriter(getPath(file.getName(), folder.getName()), StandardCharsets.UTF_8.name());

            Long count = file.getSize() * MB_TO_BYTES_FACTOR;

            while (count > 1) {
                String line = null;
                if (count < 1000) {
                    line = RandomStringUtils.randomPrint(count.intValue());
                } else {
                    line = RandomStringUtils.randomPrint(50, 1000);
                }
                writer.println(line);

                count -= line.length() + 1;
            }
        } catch (IOException e) {
            throw new FileCreationException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }



    private void updateFolder(Folder folder, Folder updateFolder) {
        Map<Long, List<File>> collect = folder.getFiles().stream()
                .collect(Collectors.groupingBy(f -> f.getSize()));

        Long sizeToCreate = updateFolder.getSize();
        if (collect.size() > 1) {
            Long minKey = Collections.min(collect.keySet());
            File fileToUpdate = collect.get(minKey).get(0);

            updateFile(updateFolder, fileToUpdate);

            sizeToCreate -= min((fileSize - minKey), sizeToCreate);
        }

        if (sizeToCreate > 0) {
            List<File> files = buildFileInfo(fileSize, sizeToCreate);
            files.forEach(file -> createFile(file, folder));
        }
    }

    private void updateFile(Folder folder, File file) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileOutputStream(
                    new java.io.File(getPath(file.getName(), folder.getName())),
                    true /* append = true */));

            Long count;
            Long spaceToUpdate = (fileSize - file.getSize());

            if (spaceToUpdate > folder.getSize()) {
                count = folder.getSize() * MB_TO_BYTES_FACTOR;
            } else {
                count = spaceToUpdate * MB_TO_BYTES_FACTOR;
            }

            while (count > 1) {
                String line;
                if (count < 1000) {
                    line = RandomStringUtils.randomPrint(count.intValue());
                } else {
                    line = RandomStringUtils.randomPrint(50, 1000);
                }
                writer.append(line);

                count -= line.length() + 1;
            }
        } catch (IOException e) {
            throw new FileCreationException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }
}
