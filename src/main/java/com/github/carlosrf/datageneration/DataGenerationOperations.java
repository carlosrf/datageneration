package com.github.carlosrf.datageneration;

/**
 * Allowed operations for the dataset.
 */
public enum DataGenerationOperations {
    CREATE,
    UPDATE,
    BACKUP
}
