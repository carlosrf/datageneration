package com.github.carlosrf.datageneration.exception;

/**
 * Exception for invalid arguments on command line
 */
public class InvalidArgumentException extends RuntimeException {
    public InvalidArgumentException(String message) {
        super(message);
    }
}
