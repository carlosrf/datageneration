package com.github.carlosrf.datageneration.exception;

/**
 * Exception for file creation errors
 */
public class FileCreationException extends RuntimeException {
    public FileCreationException(String message) {
        super(message);
    }
}
