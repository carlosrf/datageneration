package com.github.carlosrf.datageneration;

import com.github.carlosrf.datageneration.exception.InvalidArgumentException;
import com.google.common.collect.Lists;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.github.carlosrf.datageneration.util.DataGenerationUtils.buildDataGeneration;
import static com.github.carlosrf.datageneration.util.DataGenerationUtils.extractLong;
import static com.github.carlosrf.datageneration.util.DataGenerationUtils.parseFolders;

public class DataGenerationApplication {

    private static Logger logger = LoggerFactory.getLogger(DataGenerationApplication.class.getName());

    public static void main(String[] args) throws ParseException {
        logger.info("Initializing process");

        CommandLineParser parser = new DefaultParser();

        Options options = new Options();

        options.addOption(Option.builder("op")
                .longOpt("operation")
                .desc("Describe operation to be done, values must be CREATE, UPDATE or BACKUP")
                .required()
                .hasArg()
                .argName("OPERATION")
                .desc("Indicates an update on dataset")
                .build());

        options.addOption(Option.builder("l")
                .longOpt("location")
                .desc("Location where master dataset will be written.")
                .argName("PATH")
                .hasArg()
                .required()
                .build());

        options.addOption(Option.builder("t")
                .longOpt("target")
                .desc("Backup target directory")
                .argName("PATH")
                .hasArg()
                .build());

        options.addOption(Option.builder("fs")
                .longOpt("file-size")
                .desc("File size in megabytes for the file to be generated.")
                .argName("SIZE")
                .hasArg()
                .build());

        options.addOption(Option.builder("fn")
                .longOpt("folders-name")
                .desc("Folders name to be created following format <name1>,<size1>,<name2>,<size2> where <name> represents a folder name and <size> represent a size in megabytes")
                .argName("args")
                .valueSeparator(',')
                .hasArgs()
                .build());

        try {
            CommandLine line = parser.parse(options, args);

            String operationValue = line.getOptionValue("operation");
            DataGenerationOperations operation;

            try {
                operation = DataGenerationOperations.valueOf(operationValue.toUpperCase());
            } catch (Exception e) {
                throw new InvalidArgumentException("Operation argument must be CREATE, UPDATE or BACKUP");
            }

            String location = line.getOptionValue("location");

            if ((operation == DataGenerationOperations.CREATE || operation == DataGenerationOperations.UPDATE)
                && StringUtils.isBlank(line.getOptionValue("folders-name"))) {
                    throw new InvalidArgumentException("folders-name argument is mandatory for backup");
            }

            switch (operation) {
                case CREATE:
                    create(line, location, line.getOptionValues("folders-name"));
                    break;
                case UPDATE:
                    update(location, line.getOptionValues("folders-name"));
                    break;
                case BACKUP:
                    if (StringUtils.isBlank(line.getOptionValue("target"))) {
                        throw new InvalidArgumentException("target argument is mandatory for backup");
                    }
                    DataGeneration dataInfo = buildDataGeneration(location);
                    dataInfo.backup(line.getOptionValue("target"));
            }

        } catch (ParseException | InvalidArgumentException e) {
            logger.error(e.getMessage());
            new HelpFormatter().printHelp("Data Generation", options);
        }

    }

    private static void update(String location, String[] foldersArray) throws ParseException {
        DataGeneration dataInfo = buildDataGeneration(location);
        dataInfo.update(parseFolders(foldersArray));
    }

    private static void create(CommandLine line, String location, String[] foldersArray) throws ParseException {
        Long fileSize = extractLong(line.getOptionValue("file-size"), "Error on parsing file size argument");
        DataGeneration dataInfo = buildDataGeneration(fileSize, location, foldersArray);
        dataInfo.createFiles();
    }

}
