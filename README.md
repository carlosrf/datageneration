# Data Generation application

This application generates a random Master Dataset as a set of files on disk. 
 
 1. The first argument represents the location where the Master Dataset should be written.
 2. The second argument represents a file size in megabyte.
 3. The third argument has the following format <name1>,<size1>,<name2>,<size2> where <name> represents a folder name and <size> represent a size in megabytes (for example: locations,64,sensors,138,devices,24).

## Packaging

Use `mvn package` to build an executable artifact

## Usage
Executes the following command to run the application:
`java -jar target/datageneration-1.0-SNAPSHOT-jar-with-dependencies.jar <options>`

### Create
Creates entirely new dataset
`java -jar target/datageneration-1.0-SNAPSHOT-jar-with-dependencies.jar -l /opt/dataset -fs 3 -fn locations,64,sensors,138,devices,24 -op create`

### Update
Increase the folders size according to folders-name argument
`java -jar target/datageneration-1.0-SNAPSHOT-jar-with-dependencies.jar -l /opt/dataset -fn locations,12,sensors,23,devices,10 -op update`

### backup
Creates a copy of location on target, if a previous backup already exists it will be moved to another folder appended by a timestamp.
`java -jar target/datageneration-1.0-SNAPSHOT-jar-with-dependencies.jar -l /opt/dataset1 -t /opt/backup/dataset -op backup`

### Arguments options

| Options            | Arguments                            | Descrition                                            |
|--------------------|--------------------------------------|-------------------------------------------------------|
| -fn,--folders-name | <name1>,<size1>,<name2>,<size2>, ... | represents a folder name and size in megabytes        |
| -fs,--file-size    | <Integer>                            | File size in megabytes for the file to be generated.  |
| -l,--location      | /opt/dataset                         | Location where master dataset will be written.        |
| -op,--operation    | CREATE | UPDATE | BACKUP             | Indicates an update on dataset                        |
| -t,--target        | /opt/backup                          | Backup target directory                               |


## Large scale data handling

In order to handle data when it gets large we will need to make the file generation distributed. It can be done using some distributed computing environment like map/reduce.

In this case on possible solution is to use the map step to generate a file content and in reduce step store the files into the file system. It's possible to distribute it even more if necessary breaking down the files into chunks of smaller sizes and than merging it in reduce step before storing in the filesystem.

If the space necessary is too large to hold on a single machine we can use some distributed filesystem to hold the data in a distributed way.

To implement the distributed computing Hadoop or Spark framework could be used and as a distributed filesystem HDFS could be used or if using a public cloud, Amazon S3 or Google Cloud Storage also could be used.